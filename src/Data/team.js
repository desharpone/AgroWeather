// teamdata.js
import worker from "../../assets/worker.png";
const teamData = [
    {
      id: 1,
      name: "Kaaraj Wadhwa",
      position: "IoT-Engineer",
      bio: "Final Year Engineering student at Maharaja Agrasen Institute Of Technology . Major : Information Teachnology and Engineering",
      imageUrl: worker,
    },
    {
      id: 2,
      name: "Shreshth Sharma",
      position: "Team Lead",
      bio: "Third year Engineering student at Maharaja Agrasen Institute Of Technology . Major : Computer Science And Technology",
      imageUrl: worker,
    }
    // Add more team members as needed
  ];
  
  export default teamData;
  