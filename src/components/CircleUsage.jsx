import React from "react";
import styles from "./CircleUsage.module.css";

const CircularBars_Analysis = () => {
  return (
    <div className={styles["circular-bars"]}>
      <div className={styles["circle"]}>75%</div>
      <div className={styles["circle"]}>60%</div>
      <div className={styles["circle"]}>90%</div>
    </div>
  );
};

export default CircularBars_Analysis;


// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import CircularProgressbarComponent from "./CircularProgressbarComponent";

// const CircularBars_Analysis = () => {
//   const [data, setData] = useState(null);
//   const [loading, setLoading] = useState(true);
//   const [error, setError] = useState(null);
  
//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const response = await axios.get(
//           `https://sheets.googleapis.com/v4/spreadsheets/${process.env.REACT_APP_SHEET_ID}/values/Sheet1!A2:E4?key=${process.env.REACT_APP_API_KEY}`
//         );
//         setData(response.data.values);
//         setLoading(false);
//       } catch (error) {
//         setError(error);
//         setLoading(false);
//       }
//     };

//     fetchData();
//   }, []);

//   if (loading) return <div>Loading...</div>;
//   if (error) return <div>Error: {error.message}</div>;

//   return (
//     <div>
//       <CircularProgressbarComponent data={data} />
//     </div>
//   );
// };

// export default CircularBars_Analysis;
