import styles from "./SecondBox.module.css";
import InfoCard from "./InfoCard";
import flagInd from "../../assets/flagInd.png";
import worker from "../../assets/worker.png";

export default function SecondBox() {
  return (
    <div className={styles.box}>
      <h2>Connect With AgroWeather</h2>
      <InfoCard
        icon={flagInd}
        heading="India"
        ContactNo={"+91 956035XX86"}
        address={"xxx rohini sector -22 , main street , Delhi -110085"}
        className={styles["info-card"]}
      />
      <div className={styles["bottom-container"]}>
        <InfoCard
          icon={worker}
          heading="Technology"
          address={"Technology@agroweather.com"}
          ContactNo={"9899778XXX"}
          className={styles["info-card"]}
        />
        <InfoCard
          icon={worker}
          heading="Sales"
          address={"Sales@agroweather.com"}
          ContactNo={"9780778XXX"}
          className={styles["info-card"]}
        />
        <InfoCard
          icon={worker}
          heading="Managment"
          address={"Management@agroweather.com"}
          ContactNo={"7099778XXX"}
          className={styles["info-card"]}
        />
      </div>
    </div>
  );
}
