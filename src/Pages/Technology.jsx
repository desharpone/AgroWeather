import { Outlet } from "react-router-dom";
import styles from "./Technology.module.css";
import TechPanel from "../components/TechPanel.jsx";
import techData from "../Data/techData.js";
const Technology = () => {
  return (
    <>
    <Outlet />
    <div className={styles.techpage}>
      <div className={styles.tech}>
        <div className={styles.aboutTech}>
          <h2 className={styles.heading}>The Technologies Utilized In AgroWeather</h2>
          <p> AgroWeather leverages a suite of advanced technologies to provide precise and reliable weather data for agricultural purposes.
              At the core of its system is the ESP8266, a powerful Wi-Fi-enabled microcontroller that
              facilitates seamless data transmission. Temperature and humidity measurements are captured 
              using the DHT11 sensor, while atmospheric pressure is monitored by the BMP280 sensor, ensuring
              comprehensive environmental monitoring. For air quality assessment, the MQ135 sensor detects various
              harmful gases. Data from these sensors is transmitted using the MQTT protocol,
              which ensures efficient and secure communication. 
              Additionally, Node-RED is employed for data flow management and integration, providing a robust
              and scalable framework for real-time monitoring and analytics. Together, these technologies enable 
              AgroWeather to deliver accurate and actionable weather insights, enhancing agricultural productivity 
              and sustainability.
          </p>
        </div>
        <div className={styles.grid}>
          {techData.map((sensor) => (
            <TechPanel
              key={sensor.id}
              id={sensor.id}
              title={sensor.title}
              information={sensor.information}
              image={sensor.image}
            />
          ))}
        </div>
      </div>
    </div >
    </>
  );
};

export default Technology;
