import React, { useState, useEffect } from "react";
import CircularBars_Analysis from "../components/CircleUsage";
import styles from "./Analysis.module.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Analysis = () => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  const sampleData = [
    "Wheat - High yield due to optimal rainfall",
    "Corn - Good growth from consistent sunlight",
    "Soybeans - Benefited from moderate temperatures",
    "Rice - Thrived in high humidity conditions",
    "Barley - Favorable conditions with cool nights",
  ];

  useEffect(() => {
    const fetchData = async () => {
      try {
        await new Promise((resolve) => setTimeout(resolve, 1000));
        setLoading(false);
        // notify();
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  // const notify = () =>
  //   toast.success("Loading completed!", {
  //     position: toast.POSITION.BOTTOM_RIGHT,
  //   });

  if (loading)
    return (
      <>
        <div className={styles["spinner"]}>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </>
    );

  if (error) return <p>Error: {error.message}</p>;

  return (
    <div className={styles["analysis"]}>
      <ToastContainer />
      <div className={styles["head"]}>
        <h2>Average values for this quarter were: </h2>
      </div>
      <CircularBars_Analysis />
      <div className={styles["analysis-container"]}>
        <h2>Results :</h2>
        <p>* Best Crop Yields Based on Weather Conditions :</p>
        <br />
        <ul>
          {sampleData.map((value, index) => (
            <li key={index}>{value}</li>
          ))}
        </ul>
      </div>
    </div>
  );
};

export default Analysis;


// import React, { useState, useEffect } from "react";
// import dotenv from 'dotenv';
// import axios from "axios";
// import CircularBars_Analysis from "../components/CircleUsage";
// import styles from "./Analysis.module.css";
// import { ToastContainer, toast } from "react-toastify";
// import "react-toastify/dist/ReactToastify.css";

// dotenv.config();


// const Analysis = () => {
//   const [values, setValues] = useState([]);
//   const [loading, setLoading] = useState(true);
//   const [error, setError] = useState(null);
//   const spreadsheetId = process.env.REACT_APP_SHEET_ID;
//   const apiKey = process.env.REACT_APP_API_KEY;
//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const response = await axios.get(
//           `https://sheets.googleapis.com/v4/spreadsheets/${process.env.REACT_APP_SHEET_ID}/values/Sheet3!A2:A20?key=${process.env.REACT_APP_API_KEY}`
//         );
//         setValues(response.data.values || []);
//         setLoading(false);
//       } catch (error) {
//         setError(error);
//         setLoading(false);
//       }
//     };

//     fetchData();
//   }, []);
//   const notify = () =>
//     toast.success("Loading completed!", {
//       position: toast.POSITION.BOTTOM_RIGHT,
//     });

//   if (loading)
//     return (
//       <>
//         <div className={styles["spinner"]}>
//           <div></div>
//           <div></div>
//           <div></div>
//           <div></div>
//           <div></div>
//           <div></div>
//         </div>
        
//       </>
//     );
//   if (error) return <p>Error: {error.message}</p>;
  
//   return (
//     <div className={styles["analysis"]}>
//       <ToastContainer />
//       <div className={styles["head"]}>
//         <h2>Average values for this quater were: </h2>
//       </div>
//       <CircularBars_Analysis />
//       <div className={styles["analysis-container"]}>
//         <h2>Results :</h2>
//         <p>* Best Crop Yields Based on Weather Conditions :</p><br/>
//         <ul>
//           {values.map((value, index) => (
//             <li key={index}>{value}</li>
//           ))}
//         </ul>
//       </div>
//     </div>
//   );
// };

// export default Analysis;
