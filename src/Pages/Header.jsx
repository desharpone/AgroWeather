import { NavLink } from "react-router-dom";
import styles from "./Header.module.css";
import image from "../../assets/AgroWeatherLogo.jpg";

const Header = () => {
  const navLinkClass = ({ isActive }) => {
    return isActive ? styles.active : "";
  };

  return (
    <ul className={styles.Header}>
      <li className={styles.header_buttons}>
        <NavLink to="/" className={navLinkClass}>
          About
        </NavLink>
      </li>
      <li className={styles.header_buttons}>
        <NavLink to="/Dashboard" className={navLinkClass}>
          Dashboard
        </NavLink>
      </li>
      <li className={styles.header_buttons}>
        <NavLink to="/Analysis" className={navLinkClass}>
          Analysis
        </NavLink>
      </li>
      <li className={styles.header_symbols}>
        <a href="/">
          <img src={image} alt="AgroWeather" />
        </a>
      </li>
      <li className={styles.header_buttons}>
        <NavLink to="/Technology" className={navLinkClass}>
          Technology
        </NavLink>
      </li>
      <li className={styles.header_buttons}>
        <NavLink to="/Faq" className={navLinkClass}>
          Faq
        </NavLink>
      </li>
      <li className={`${styles.header_buttons} ${styles.git}`}>
        <NavLink to="/ContactUs" className={navLinkClass}>
          Contact Us
        </NavLink>
      </li>
    </ul>
  );
};

export default Header;

